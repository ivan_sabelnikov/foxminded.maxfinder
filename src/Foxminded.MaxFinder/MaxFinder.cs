﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxminded.MaxFinder
{
    public class MaxFinder
    {
        private const string NumbersSeparator = ",";

        public async Task<MaxFinderResult> Find(StreamReader streamReader)
        {
            var fakeLines = new List<int>();
            var maxSum = double.MinValue;
            var maxSumLineIndex = -1;
            var lineIndex = -1;

            while (!streamReader.EndOfStream)
            {
                var line = await streamReader.ReadLineAsync();

                lineIndex++;

                if (!TryParseLine(line, out var sum))
                {
                    fakeLines.Add(lineIndex);
                }

                if (sum > maxSum)
                {
                    maxSum = sum;
                    maxSumLineIndex = lineIndex;
                }
            }

            return new MaxFinderResult
            {
                MaxLineIndex = maxSumLineIndex < 0 ? (int?) null : maxSumLineIndex,
                FakeLines = fakeLines.ToArray()
            };
        }

        private bool TryParseLine(string line, out double sum)
        {
            sum = 0;

            foreach (var item in line.Split(NumbersSeparator).Select(x => x.Trim()))
            {
                if (!double.TryParse(item, out var number))
                {
                    sum = default;
                    return false;
                }

                sum += number;
            }

            return true;
        }
    }
}
