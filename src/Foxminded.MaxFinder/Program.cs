﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Foxminded.MaxFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            string path;

            if (args.Length > 0)
            {
                path = args[0];
            }
            else
            {
                Console.Write("Enter file path: ");
                path = Console.ReadLine();
            }

            try
            {
                var result = new MaxFinder()
                    .Find(new StreamReader(path))
                    .GetAwaiter()
                    .GetResult();

                Console.WriteLine($"Result: {JsonConvert.SerializeObject(result)}");
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine($"Error occured: {exception}");
            }
        }
    }
}
