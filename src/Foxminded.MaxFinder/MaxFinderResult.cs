﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Foxminded.MaxFinder
{
    public class MaxFinderResult
    {
        public int? MaxLineIndex { get; set; }

        public int[] FakeLines { get; set; } = Array.Empty<int>();
    }
}
