using FluentAssertions;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Foxminded.MaxFinder.Tests
{
    public class MaxFinderTests
    {
        [Fact]
        public async Task EmptyStream_EmptyResult()
        {
            var result = await RunFinder(string.Empty);

            result.FakeLines.Length.Should().Be(0);
            result.MaxLineIndex.Should().BeNull();
        }

        [Fact]
        public async Task OneLine_ReturnMax()
        {
            var result = await RunFinder("15.5");

            result.MaxLineIndex.Should().Be(0);
        }

        [Fact]
        public async Task OneFakeLine_ReturnFakeLine()
        {
            var result = await RunFinder("1f5.5");

            result.FakeLines.Should().BeEquivalentTo(new[] { 0 });
        }

        [Fact]
        public async Task SeveralLines_ReturnMax()
        {
            var input = Join("15.5, 2", "2,20.5", "0, 20.1, .5, 0.00001");

            var result = await RunFinder(input);

            result.MaxLineIndex.Should().Be(2);
        }

        [Fact]
        public async Task SeveralFakeLines_ReturnAllFakeLines()
        {
            var input = Join("1d5.5, 2", "2, 20.5, hello", "*0, 50", "45", "5, a, 6", "", "5,", " ");

            var result = await RunFinder(input);

            result.FakeLines.Should().BeEquivalentTo(new[] { 0, 1, 2, 4, 5, 6, 7 });
        }

        [Fact]
        public async Task OneValidLine_NotParsedAsFake()
        {
            var result = await RunFinder("5.5,\t2 , 5.4587954,    15456546.4564654 ,\t 6465");

            result.FakeLines.Length.Should().Be(0);
        }

        private static async Task<MaxFinderResult> RunFinder(string inputString)
        {
            var inputBytes = Encoding.Default.GetBytes(inputString);
            var memoryStream = new MemoryStream(inputBytes);

            return await new MaxFinder().Find(new StreamReader(memoryStream));
        }

        private static string Join(params string[] parts)
        {
            return string.Join(Environment.NewLine, parts);
        }
    }
}